import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { CandidateManagementComponent } from './components/candidate-management/candidate-management.component';
import { AuthGuard } from './service/auth.guard';
import { GradEditedHistoryComponent } from './components/grad-edited-history/grad-edited-history.component';


const routes: Routes = [
  {path:"", component: LoginComponent},
  {path:"candidate-management",component:CandidateManagementComponent,canActivate: [AuthGuard]},
  {path: "edited-history/:id", component: GradEditedHistoryComponent,canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
