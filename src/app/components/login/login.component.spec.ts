import { async, ComponentFixture, TestBed, getTestBed, fakeAsync, tick } from '@angular/core/testing';

import { LoginComponent, provideConfig } from './login.component';
import { AppModule } from 'src/app/app.module';
import { AuthService, SocialUser, SocialLoginModule, AuthServiceConfig } from 'angularx-social-login';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { MakeHttpService } from 'src/app/service/make-http.service';
import { Observable, of } from 'rxjs';
import { AuthGuard } from 'src/app/service/auth.guard';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import {delay} from 'rxjs/operators';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';
describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let injector: TestBed;
  let authService: AuthService;
  let httpMock:HttpTestingController;
  let service: MakeHttpService;
  let guard: AuthGuard;
  let dialog:MatDialog;
  let routerMock;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule,HttpClientTestingModule],
      declarations: [ LoginComponent ],
      providers: [AuthService, SocialLoginModule ,
        {
            provide: AuthServiceConfig,
            useFactory: provideConfig
          },
    ]
    })
    .compileComponents();
    injector = getTestBed();
    authService = injector.get(AuthService);
    service = TestBed.inject(MakeHttpService);
    httpMock = TestBed.inject(HttpTestingController);
    guard = injector.get(AuthGuard);
    dialog = injector.get(MatDialog);
    routerMock = injector.get(Router);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });
  afterEach(()=> {
    httpMock.verify();
  })
  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should redirect an authenticated user to the login route',() => {
    const googleUser: SocialUser = {
      name: 'testname', firstName: 'first',
      lastName: 'last', idToken: '1009',
      provider: 'google',
      id: '2', email: 'test@gmail.com',
      photoUrl: 'null',
      authToken: '2323', authorizationCode: '232'
    };
    spyOn(authService, 'signIn').and.returnValue(Promise.resolve(googleUser));
    
  const data = 1;
  spyOn(service,'makePostCall').and.returnValue(of(data));
  spyOn(routerMock,"navigate").and.returnValue(routerMock.navigate(['/candidate-management']));
  component.signInWithGoogle();
  expect(routerMock.navigate).toHaveBeenCalledWith(['/candidate-management']); 
  component.dialog.closeAll();
  });
});
