import { Component, OnInit } from '@angular/core';
import { AuthServiceConfig, GoogleLoginProvider, AuthService } from 'angularx-social-login';
import { Router } from '@angular/router';
import { AppComponent } from '../../app.component';
import { MainNavComponent } from '../main-nav/main-nav.component';
import { MakeHttpService } from '../../service/make-http.service';
import { MatDialog } from '@angular/material/dialog';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';
import { environment } from 'src/environments/environment';

const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("622766730529-el94n036l9vcpppcqe643ltmg0stdo2t.apps.googleusercontent.com")
  }
]);
 
export function provideConfig() {
  return config;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService:AuthService,private router:Router,private apiCall:MakeHttpService,public dialog: MatDialog) { }

  ngOnInit(): void {
  }
  
  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(userData => {
      const progressDialog = this.dialog.open(ProgressDialogComponent,{
        width:"400px",
        disableClose: true ,
        data: "Signing in AU Management System..."
      })
      console.log(userData)
         localStorage.setItem('email',userData.email);
         localStorage.setItem('idToken',userData.idToken);
         localStorage.setItem('photoUrl',userData.photoUrl);
         localStorage.setItem('name',userData.name);
         localStorage.setItem('firstName',userData.firstName);
         localStorage.setItem('lastName',userData.lastName);
          this.apiCall.makePostCall(environment.loginUrl,
            {"userEmail":localStorage.getItem('email'), "userName":localStorage.getItem('name')}).subscribe(loginStatus =>{
             progressDialog.close();
            // this.router.navigateByUrl("/candidate-management");
            this.router.navigate(['/candidate-management']);
            })
    });
  
 }

}
