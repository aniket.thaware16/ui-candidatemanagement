import { Component, OnInit, ViewChild } from '@angular/core';
import { MakeHttpService } from 'src/app/service/make-http.service';
import { MatDialog } from '@angular/material/dialog';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';
import { environment } from 'src/environments/environment';
import { GradDetailsComponent } from '../grad-details/grad-details.component';
import { ActivatedRoute } from '@angular/router';
import { AgGridAngular } from 'ag-grid-angular';
import { CustomHistoryCellComponent } from '../custom-history-cell/custom-history-cell.component';
import { GridOptions } from 'ag-grid-community';

@Component({
  selector: 'app-grad-edited-history',
  templateUrl: './grad-edited-history.component.html',
  styleUrls: ['./grad-edited-history.component.css']
})
export class GradEditedHistoryComponent implements OnInit {
  @ViewChild('agGrid') agGrid: AgGridAngular;
  frameworkComponents:any;
 columnDefs = [
    {headerName: 'Email', field: 'email',sortable: true , filter: true},
    {headerName: 'Name', field: 'firstName' ,sortable: true, filter: true},
    {headerName: 'Gender', field: 'gender',sortable: true, filter: true},
    {headerName: 'Contact No.', field: 'contactNo',sortable: true, filter: true},
    {headerName: 'Institute Name', field: 'instituteName',sortable: true, filter: true},
    {headerName: 'Joining Location', field: 'joiningLocation',sortable: true, filter: true},
    {headerName: 'Joining Date', field: 'joiningDate',sortable: true, filter: true},
    {headerName: 'Show Details', field: 'id',cellRenderer:"CustomHistoryCellComponent"}
];

   rowData = [];
  gridOptions: any;

  constructor(private apiCall: MakeHttpService, private dialog: MatDialog, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getGradEditedHistory();
    this.gridOptions = <GridOptions>{
      context: {
          componentParent: this
      }
  };
    this.frameworkComponents = {
      CustomHistoryCellComponent: CustomHistoryCellComponent
    }
  }

  getGradEditedHistory() {
    const parentId = +this.route.snapshot.paramMap.get('id');
    let gradHistoryUrl = environment.gradEditedHistoryUrl + "/"+parentId;
    this.apiCall.makeGetCall(gradHistoryUrl).subscribe((result) => {
        this.rowData = result;
    });
  }

  showDetailsOfGrad(gradId: number) {
    const progressDialog = this.dialog.open(ProgressDialogComponent,{
      width:"400px",
      disableClose: true ,
      data: "Fetchng Details of Grad"
    })
    let gradDetailsUrl = environment.gradUrl+"/"+gradId;
    this.apiCall.makeGetCall(gradDetailsUrl).subscribe(gradDetails=>{
      progressDialog.close();
      const gradDetailsDialog = this.dialog.open(GradDetailsComponent,{
        width: '500px',
        data: gradDetails
      })
    })
  }
}
