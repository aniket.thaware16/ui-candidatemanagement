import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GradEditedHistoryComponent } from './grad-edited-history.component';
import { AppModule } from 'src/app/app.module';
import { MakeHttpService } from 'src/app/service/make-http.service';
import { Grad } from 'src/app/models/grad.model';
import { of } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { GradDetailsComponent } from '../grad-details/grad-details.component';

describe('GradEditedHistoryComponent', () => {
  let component: GradEditedHistoryComponent;
  let fixture: ComponentFixture<GradEditedHistoryComponent>;
  let service: MakeHttpService;
  let dialog:MatDialog;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [ GradEditedHistoryComponent ]
    })
    .compileComponents();
    service = TestBed.inject(MakeHttpService);
    dialog = TestBed.inject(MatDialog);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GradEditedHistoryComponent);
    component = fixture.componentInstance;
    //fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call the getGradEditedHistory() method on calling ngOnInIt', () => {
    spyOn(component,'getGradEditedHistory').and.returnValue();
    component.ngOnInit();
    expect(component.getGradEditedHistory).toHaveBeenCalled();
  });

  it('should set the rowData on calling getGradEditedHistory() method', () => {
    const dummyGrads:Grad[] = [
      {
        id: 35,
        firstName: 'f7name',
        middleName: 'm7Name',
        lastName: 'l7Name',
        gender: 'Male',
        email: 'grad7@gmail.com',
        contactNo: '7743863042',
        instituteName: 'VJTI,Mumbai',
        degreeName: 'B.Tech',
        branchName: 'Electronics and Telecommunication',
        joiningDate: '2020-06-03',
        joiningLocation: 'Banglore',
        feedback: '',
        createdOn: new  Date ("2020-06-11"),
        state: '',
        city: '',
        street: '',
        zipCode: '',
        houseNo: '',
        createdByUser: 'test@accoliteindia.com',
        parentId: 35,
        skills: null,
      },
      {
        id: 22,
        firstName: 'f5Name',
        middleName: 'm5Name',
        lastName: 'l5Name',
        gender: 'Male',
        email: 'grad6@gmail.com',
        contactNo: '8877442560',
        instituteName: 'College of Engineering,Pune',
        degreeName: 'B.Tech',
        branchName: 'Electronics and Telecommunication',
        joiningDate: '2020-05-01',
        joiningLocation: 'Hyderabad',
        feedback: '',
        createdOn: new  Date ("2020-06-11"),
        state: '',
        city: '',
        street: '',
        zipCode: '',
        houseNo: '',
        createdByUser: 'test@accoliteindia.com',
        parentId: 22,
        skills: null,
      },
      {
        id: 5,
        firstName: 'f5Name',
        middleName: 'm5Name',
        lastName: 'l5Name',
        gender: 'Male',
        email: 'grad5@gmail.com',
        contactNo: '8899774466',
        instituteName: 'Vishwakarma Institute of Technology,Pune',
        degreeName: 'M.Tech',
        branchName: 'Information Technology',
        joiningDate: '2020-06-11',
        joiningLocation: 'Delhi',
        feedback: '',
        createdOn: new  Date ("2020-06-11"),
        state: '',
        city: '',
        street: '',
        zipCode: '',
        houseNo: '',
        createdByUser: 'test@accoliteindia.com',
        parentId: 5,
        skills: null,
      },
      {
        id: 4,
        firstName: 'f4Name',
        middleName: 'm4Name',
        lastName: 'l4Name',
        gender: 'Female',
        email: 'grad4@gmail.com',
        contactNo: '7748965230',
        instituteName: 'College of Engineering,Pune',
        degreeName: 'M.Tech',
        branchName: 'Information Technology',
        joiningDate: '2020-06-02',
        joiningLocation: 'Banglore',
        feedback: '',
        createdOn: new  Date ("2020-06-11"),
        state: '',
        city: '',
        street: '',
        zipCode: '',
        houseNo: '',
        createdByUser: 'test@accoliteindia.com',
        parentId: 4,
        skills: null,
      }
    ];
    spyOn(service,'makeGetCall').and.returnValue(of(dummyGrads));
    component.getGradEditedHistory();
    expect(component.rowData).toEqual(dummyGrads);
  });

  it("should open gradDetails component on calling showDetailsOfGrad()",()=>{
    const dummyGrad = {
      id: 1,
      firstName: 'f4Name',
      middleName: 'm4Name',
      lastName: 'l4Name',
      gender: 'Female',
      email: 'grad4@gmail.com',
      contactNo: '7748965230',
      instituteName: 'College of Engineering,Pune',
      degreeName: 'M.Tech',
      branchName: 'Information Technology',
      joiningDate: '2020-06-02',
      joiningLocation: 'Banglore',
      feedback: '',
      createdOn: new  Date ("2020-06-11"),
      state: '',
      city: '',
      street: '',
      zipCode: '',
      houseNo: '',
      createdByUser: 'test@accoliteindia.com',
      parentId: 4,
      skills: [{id:1,
        skillName:"Angular",
        createdOn:new Date(),
    createdByUser:"test@gmail.com"
        }]
    };
    spyOn(service,"makeGetCall").and.returnValue(of(dummyGrad));
    spyOn(dialog,'open').and.callThrough();
    component.showDetailsOfGrad(1);
    expect(dialog.open).toHaveBeenCalledWith(GradDetailsComponent,{
      width: '500px',
      data: dummyGrad
    })
    dialog.closeAll();
  })
});
