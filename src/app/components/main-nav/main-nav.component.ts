import { Component, OnInit } from '@angular/core';
import { AuthService } from 'angularx-social-login';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent implements OnInit {
  user: any;

  constructor(private authService:AuthService,private router:Router) { }

  ngOnInit(): void {
    this.user = {
        "name":localStorage.getItem('name'),
        "photoUrl": localStorage.getItem('photoUrl')
    }
  }


  signOut(){
      this.authService.signOut();
      localStorage.clear();
      this.router.navigate([""]);
  }

}
