import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainNavComponent } from './main-nav.component';
import { AppModule } from 'src/app/app.module';
import { AuthService } from 'angularx-social-login';
import { Router } from '@angular/router';

describe('MainNavComponent', () => {
  let component: MainNavComponent;
  let fixture: ComponentFixture<MainNavComponent>;
  let authService: AuthService;
  let router: Router;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [ MainNavComponent ]
    })
    .compileComponents();
    authService = TestBed.inject(AuthService);
    router = TestBed.inject(Router);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainNavComponent);
    component = fixture.componentInstance;
    //fixture.detectChanges();
  });

  it('should create', () => {
   let dummyUser = {
      "name":"test",
      "photoUrl": "12345"
  }
  spyOn(localStorage,'getItem').and.callFake(function(arg) {
    if (arg === 'name'){
        return 'test';
    } else if(arg === 'photoUrl') {
        return '12345';
    }
});
    component.ngOnInit();
    expect(component.user).toEqual(dummyUser);
    expect(component).toBeTruthy();
  });

  it('should clear localStorage and route to login after signOut', () => {
    localStorage.setItem('name','test');
    spyOn(authService, 'signOut').and.returnValue(Promise.resolve(""));
    spyOn(router,'navigate');
    component.signOut();
    expect(router.navigate).toHaveBeenCalledWith([""]);
   });
});
