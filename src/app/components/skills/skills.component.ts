import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { MakeHttpService } from 'src/app/service/make-http.service';
import { environment } from 'src/environments/environment';
import { Skill } from 'src/app/models/skill.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.css']
})
export class SkillsComponent implements OnInit {
  skilladdedMessage = "";
  skillForm = new FormGroup({
    skillName: new FormControl('')
  })
  skills: Array<Skill>;
  constructor( public dialogRef: MatDialogRef<SkillsComponent>,private apiCall:MakeHttpService,private dialog:MatDialog) { }

  ngOnInit(): void {
    this.getAllSkills();
  }
  getAllSkills(){
    this.apiCall.makeGetCall(environment.skillUrl).subscribe(allSkills => {
      this.skills = allSkills;
    })
}

addNewSkill(skillName:string){
 let skill = {
   "skillName": skillName,
   "createdByUser": localStorage.getItem('email')
 }
const progressDialog = this.dialog.open(ProgressDialogComponent,{
  width:"400px",
      disableClose: true ,
  data: "Saving Skill"
})
  this.apiCall.makePostCall(environment.skillUrl,skill).subscribe(result =>{
    progressDialog.close();
     if(result==0){
        this.skilladdedMessage = skillName+ " Skill is already present in the Skills"
     }else{
      this.skilladdedMessage = "";
      this.skillForm.get('skillName').setValue("")
      this.getAllSkills(); 
     }
    
      
  })
}
}
