import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillsComponent } from './skills.component';
import { AppModule } from 'src/app/app.module';
import { MakeHttpService } from 'src/app/service/make-http.service';
import { of } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

describe('SkillsComponent', () => {
  let component: SkillsComponent;
  let fixture: ComponentFixture<SkillsComponent>;
  let service: MakeHttpService;
  let dialog:MatDialog;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [SkillsComponent],
    }).compileComponents();
    service = TestBed.inject(MakeHttpService);
    dialog = TestBed.inject(MatDialog);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillsComponent);
    component = fixture.componentInstance;
    //fixture.detectChanges();
  });

  it('should create', () => {
    spyOn(component, 'getAllSkills').and.returnValue();
    component.ngOnInit();
    expect(component).toBeTruthy();
  });

  it('should get All Skills on calling getAllSkills()', () => {
    const dummySkills = [
      {
        id: 1,
        skillName: 'Angular',
        createdOn: new Date(),
        createdByUser: 'test@gmail.com',
      },
      {
        id: 2,
        skillName: 'Html5',
        createdOn: new Date(),
        createdByUser: 'test@gmail.com',
      }
    ];
    spyOn(service, 'makeGetCall').and.returnValue(of(dummySkills));
    component.getAllSkills();
    expect(component.skills).toEqual(dummySkills);
  });

  it('should set the skilladdedMessage if skill is already present in database', () => {
    spyOn(service, 'makePostCall').and.returnValue(of(0));
    component.addNewSkill("Angular");
    expect(component.skilladdedMessage).toContain("Angular");
    dialog.closeAll();
  });

  it('should reset the skilladdedMessage and skillForm name value if skill is not present in database', () => {
    spyOn(service, 'makePostCall').and.returnValue(of(1));
    spyOn(component,'getAllSkills').and.returnValue();
    component.addNewSkill("Angular");
    expect(component.skilladdedMessage).toEqual("");
    expect(component.skillForm.get('skillName').value).toEqual("");
    dialog.closeAll();
  });
});
