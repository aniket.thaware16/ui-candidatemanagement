import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrendsComponent } from './trends.component';
import { AppModule } from 'src/app/app.module';
import { MakeHttpService } from 'src/app/service/make-http.service';
import { of } from 'rxjs';
import * as Chart from 'chart.js';
import { MatDialog } from '@angular/material/dialog';

describe('TrendsComponent', () => {
  let component: TrendsComponent;
  let fixture: ComponentFixture<TrendsComponent>;
  let service: MakeHttpService;
  let dialog:MatDialog;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[AppModule],
      declarations: [ TrendsComponent ]
    })
    .compileComponents();
    service = TestBed.inject(MakeHttpService);
    dialog = TestBed.inject(MatDialog);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrendsComponent);
    component = fixture.componentInstance;
   // fixture.detectChanges();
  });

  it('should create', () => {
    component.ngOnInit();
    expect(component).toBeTruthy();
  });

  it('should create chart on calling getTrends()', () => {
    let dummyTrends = [{
      name: "Angular",
      count: 10
    },
    {
      name: "Html",
      count: 10
    }]
    spyOn(service,'makeGetCall').and.returnValue(of(dummyTrends));
    component.getTrendsData("Skill",2020);
    expect(component.chart).toBeInstanceOf(Chart);
    dialog.closeAll();
  });
});
