import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MakeHttpService } from '../../service/make-http.service';
import { Chart } from 'chart.js';
import { tick } from '@angular/core/testing';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-trends',
  templateUrl: './trends.component.html',
  styleUrls: ['./trends.component.css'],
})
export class TrendsComponent implements OnInit {
  chart: any;
  canvas:any;
  name = [];
  count = [];
  yearData = [2019,2020];
  trendType = ["Location","Skills"];
  constructor(
    public dialogRef: MatDialogRef<TrendsComponent>,
    private apiCall: MakeHttpService
  ) {}

  ngOnInit(): void {

  }

  getTrendsData(typeName: string, year: number) {
    let trendsUrl = environment.trendsUrl + '/' + typeName + '/' + year;

    this.apiCall.makeGetCall(trendsUrl).subscribe((trends) => {
      this.name = []
      this.count = []
      for (let trend of trends) {
        this.name.push(trend.name);
        this.count.push(trend.count);
      }
      this.chart = new Chart( 'canvas', {
        type: 'bar',
        data: {
          labels: this.name,
          datasets: [
            {
              label: 'Number of Grads',
              data: this.count,
              backgroundColor: '#3F51B5',
              borderColor: '#3F51B5',
              borderWidth: 1
            },
          ],
        },
        options: { 
          responsive: true,
          legend: {
            position: 'top',
          },
          title:{
            display: true,
            text: year+':Number of Grads '+typeName+'wise'
          },
          scales:{
            yAxes:[
              {ticks:{beginAtZero:true,stepSize: 1}}
            ]
          }
         },
      });
    });
  }
}
