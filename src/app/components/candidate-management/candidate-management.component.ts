import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Grad } from '../../models/grad.model';
import { MatDialog } from '@angular/material/dialog';
import { AddGradComponent } from '../add-grad/add-grad.component';
import { MakeHttpService } from '../../service/make-http.service';
import { GradDetailsComponent } from '../grad-details/grad-details.component';
import { TrendsComponent } from '../trends/trends.component';
import {MatSort} from '@angular/material/sort';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';
import { environment } from 'src/environments/environment';
import { SkillsComponent } from '../skills/skills.component';
import { AgGridAngular } from 'ag-grid-angular';
import { GridOptions } from 'ag-grid-community';
import { CustomGradCellComponent } from '../custom-grad-cell/custom-grad-cell.component';
@Component({
  selector: 'app-candidate-management',
  templateUrl: './candidate-management.component.html',
  styleUrls: ['./candidate-management.component.css'],
})
export class CandidateManagementComponent implements OnInit {
  @ViewChild('agGrid') agGrid: AgGridAngular;
  frameworkComponents:any;
 columnDefs = [
    {headerName: 'Email', field: 'email',sortable: true , filter: true},
    {headerName: 'Name', field: 'firstName' ,sortable: true, filter: true},
    {headerName: 'Gender', field: 'gender',width: 150,sortable: true, filter: true},
    {headerName: 'Contact No.', field: 'contactNo',width: 200,sortable: true, filter: true},
    {headerName: 'Institute Name', field: 'instituteName',width: 300,sortable: true, filter: true},
    {headerName: 'Joining Location', field: 'joiningLocation',width: 180,sortable: true, filter: true},
    {headerName: 'Joining Date', field: 'joiningDate',width: 150,sortable: true, filter: true},
    {headerName: '', field: '',cellRenderer:"CustomGradCellComponent"}
];

   rowData = [];
  gridOptions: any;
  constructor(private apiCall: MakeHttpService, public dialog: MatDialog) {}

  ngOnInit(): void {
    this.getAllGrads();
    this.gridOptions = <GridOptions>{
      context: {
          componentParent: this
      }
  };
    this.frameworkComponents = {
      CustomGradCellComponent: CustomGradCellComponent
    }
  }

  

  openGradForm() {
    const dialogRef = this.dialog.open(AddGradComponent, {
      data: { isEdit: false, grad: null },
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.getAllGrads();
    });
  }

  showTrends(){
      const trendsDialog = this.dialog.open(TrendsComponent);
  }

  getAllGrads() {
    this.apiCall.makeGetCall(environment.gradUrl).subscribe((result) => {
      this.rowData = result;
    });
  }

  showDetailsOfGrad(gradId: number) {
    const progressDialog = this.dialog.open(ProgressDialogComponent,{
      width:"400px",
      disableClose: true ,
      data: "Fetchng Details of Grad"
    })
    let gradDetailsUrl = environment.gradUrl+"/"+gradId;
    this.apiCall.makeGetCall(gradDetailsUrl).subscribe(gradDetails=>{
      progressDialog.close();
      const gradDetailsDialog = this.dialog.open(GradDetailsComponent,{
        width: '500px',
        data: gradDetails
      })
    })
  }

  editGrad(gradId: number) {
    let gradUrl = environment.gradUrl + "/" + gradId;
    this.apiCall.makeGetCall(gradUrl).subscribe((gradDetail) => {
      const dialogRef = this.dialog.open(AddGradComponent, {
        data: { isEdit: true, grad: gradDetail },
      });
      dialogRef.afterClosed().subscribe((result) => {
        this.getAllGrads();
      });
    });
  
  }
  
  deleteGrad(gradId: number) {
    const progressDialog = this.dialog.open(ProgressDialogComponent,{
      width:"400px",
      disableClose: true ,
      data: "Deleting Grad"
    })
    let deleteUrl = environment.gradUrl+"/"+gradId;
    this.apiCall.makeDeleteCall(deleteUrl).subscribe(deleteMsg =>{
      progressDialog.close();
      this.getAllGrads();
    })
  }

  manageSkills(){
    const manageSkillDialog = this.dialog.open(SkillsComponent)
  }
}
