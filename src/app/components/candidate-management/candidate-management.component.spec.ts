import {
  async,
  ComponentFixture,
  TestBed,
  fakeAsync,
  tick,
  getTestBed,
} from '@angular/core/testing';

import { CandidateManagementComponent } from './candidate-management.component';
import { AppModule } from 'src/app/app.module';
import { Grad } from 'src/app/models/grad.model';
import { MakeHttpService } from 'src/app/service/make-http.service';
import { of } from 'rxjs';
import { SkillsComponent } from '../skills/skills.component';
import { TrendsComponent } from '../trends/trends.component';
import { GradDetailsComponent } from '../grad-details/grad-details.component';
import { AddGradComponent } from '../add-grad/add-grad.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

describe('CandidateManagementComponent', () => {
  let component: CandidateManagementComponent;
  let fixture: ComponentFixture<CandidateManagementComponent>;
  let service: MakeHttpService;
  let dialog: MatDialog;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [CandidateManagementComponent],
    }).compileComponents();
    service = TestBed.inject(MakeHttpService);
    dialog = TestBed.inject(MatDialog);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidateManagementComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create', () => {
    spyOn(component, 'getAllGrads').and.returnValue();
    component.ngOnInit();
    expect(component).toBeTruthy();
    component.dialog.closeAll();
  });
  it('should assign data to rowData property', () => {
    const dummyGrads = [
      {
        id: 1,
        firstName: 'f4Name',
        middleName: 'm4Name',
        lastName: 'l4Name',
        gender: 'Female',
        email: 'grad4@gmail.com',
        contactNo: '7748965230',
        instituteName: 'College of Engineering,Pune',
        degreeName: 'M.Tech',
        branchName: 'Information Technology',
        joiningDate: '2020-06-02',
        joiningLocation: 'Banglore',
        feedback: '',
        createdOn: new Date('2020-06-11'),
        state: '',
        city: '',
        street: '',
        zipCode: '',
        houseNo: '',
        createdByUser: 'test@accoliteindia.com',
        parentId: 4,
        skills: [
          {
            id: 1,
            skillName: 'Angular',
            createdOn: new Date(),
            createdByUser: 'test@gmail.com',
          },
        ],
      },
      {
        id: 2,
        firstName: 'f4Name',
        middleName: 'm4Name',
        lastName: 'l4Name',
        gender: 'Female',
        email: 'grad4@gmail.com',
        contactNo: '7748965230',
        instituteName: 'College of Engineering,Pune',
        degreeName: 'M.Tech',
        branchName: 'Information Technology',
        joiningDate: '2020-06-02',
        joiningLocation: 'Banglore',
        feedback: '',
        createdOn: new Date('2020-06-11'),
        state: '',
        city: '',
        street: '',
        zipCode: '',
        houseNo: '',
        createdByUser: 'test@accoliteindia.com',
        parentId: 4,
        skills: [
          {
            id: 1,
            skillName: 'Angular',
            createdOn: new Date(),
            createdByUser: 'test@gmail.com',
          },
        ],
      },
    ];
    spyOn(service, 'makeGetCall').and.returnValue(of(dummyGrads));
    component.getAllGrads();
    expect(component.rowData).toEqual(dummyGrads);
    component.dialog.closeAll();
  });

  it('should delete grad', () => {
    spyOn(component, 'getAllGrads').and.returnValue();
    spyOn(service, 'makeDeleteCall').and.returnValue(of(1));
    component.deleteGrad(1);
    expect(component.getAllGrads).toHaveBeenCalled();
  });

  it('should open skill component dialog', () => {
    const dummySkillDialog = dialog.open(SkillsComponent);
    spyOn(component.dialog, 'open').and.returnValue(dummySkillDialog);
    component.manageSkills();
    expect(component.dialog.open).toHaveBeenCalledWith(SkillsComponent);
  });

  it('should open trends component dialog', () => {
    const dummyTrendsDialog = dialog.open(TrendsComponent);
    spyOn(component.dialog, 'open').and.returnValue(dummyTrendsDialog);
    component.showTrends();
    expect(component.dialog.open).toHaveBeenCalledWith(TrendsComponent);
  });

  it('should open details grad component dialog', () => {
    const dummyGrad = {
      id: 1,
      firstName: 'f4Name',
      middleName: 'm4Name',
      lastName: 'l4Name',
      gender: 'Female',
      email: 'grad4@gmail.com',
      contactNo: '7748965230',
      instituteName: 'College of Engineering,Pune',
      degreeName: 'M.Tech',
      branchName: 'Information Technology',
      joiningDate: '2020-06-02',
      joiningLocation: 'Banglore',
      feedback: '',
      createdOn: new Date('2020-06-11'),
      state: '',
      city: '',
      street: '',
      zipCode: '',
      houseNo: '',
      createdByUser: 'test@accoliteindia.com',
      parentId: 4,
      skills: [
        {
          id: 1,
          skillName: 'Angular',
          createdOn: new Date(),
          createdByUser: 'test@gmail.com',
        },
      ],
    };
    spyOn(service, 'makeGetCall').and.returnValue(of(dummyGrad));
    const dummyDetailsGradDialog = dialog.open(GradDetailsComponent, {
      width: '500px',
      data: dummyGrad,
    });
    spyOn(component.dialog, 'open').and.returnValue(dummyDetailsGradDialog);
    component.showDetailsOfGrad(1);
    expect(component.dialog.open).toHaveBeenCalledWith(GradDetailsComponent, {
      width: '500px',
      data: dummyGrad,
    });
  });

  it('should open edit grad component dialog', () => {
    const dummyGrad = {
      id: 1,
      firstName: 'f4Name',
      middleName: 'm4Name',
      lastName: 'l4Name',
      gender: 'Female',
      email: 'grad4@gmail.com',
      contactNo: '7748965230',
      instituteName: 'College of Engineering,Pune',
      degreeName: 'M.Tech',
      branchName: 'Information Technology',
      joiningDate: '2020-06-02',
      joiningLocation: 'Banglore',
      feedback: '',
      createdOn: new Date('2020-06-11'),
      state: '',
      city: '',
      street: '',
      zipCode: '',
      houseNo: '',
      createdByUser: 'test@accoliteindia.com',
      parentId: 4,
      skills: [
        {
          id: 1,
          skillName: 'Angular',
          createdOn: new Date(),
          createdByUser: 'test@gmail.com',
        },
      ],
    };
    spyOn(service, 'makeGetCall').and.returnValue(of(dummyGrad));
    const editDialog = component.dialog.open(AddGradComponent, {
      data: { isEdit: true, grad: dummyGrad },
    });
    spyOn(component.dialog, 'open').and.returnValue(editDialog);
    spyOn(editDialog, 'afterClosed').and.returnValue(of({ result: 'closed' }));
    spyOn(component, 'getAllGrads').and.returnValue();
    component.editGrad(1);
    expect(component.dialog.open).toHaveBeenCalledWith(AddGradComponent, {
      data: { isEdit: true, grad: dummyGrad },
    });
    component.dialog.closeAll();
  });

  it('should open add grad component dialog', () => {
    const addDialog = component.dialog.open(AddGradComponent, {
      data: { isEdit: false, grad: null },
    });
    spyOn(component.dialog, 'open').and.returnValue(addDialog);
    spyOn(addDialog, 'afterClosed').and.returnValue(of({ result: 'closed' }));
    spyOn(component, 'getAllGrads').and.returnValue();
    component.openGradForm();
    expect(component.dialog.open).toHaveBeenCalledWith(AddGradComponent, {
      data: { isEdit: false, grad: null },
    });
  });
});
