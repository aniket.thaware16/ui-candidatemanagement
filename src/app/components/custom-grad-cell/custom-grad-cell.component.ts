import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-custom-grad-cell',
  templateUrl: './custom-grad-cell.component.html',
  styleUrls: ['./custom-grad-cell.component.css']
})
export class CustomGradCellComponent implements OnInit,ICellRendererAngularComp {
  params:any;
  constructor() { }
  refresh(params: any): boolean {
    this.params = params;
    return true;
  }
  agInit(params: import("ag-grid-community").ICellRendererParams): void {
    this.params = params;
  }
 
  ngOnInit(): void {
  }

  editGrad(){
    this.params.context.componentParent.editGrad(this.params.node.data.id);
  }
  deleteGrad(){
    this.params.context.componentParent.deleteGrad(this.params.node.data.id);
  }

  showDetails(){
    this.params.context.componentParent.showDetailsOfGrad(this.params.node.data.id);
  }

}
