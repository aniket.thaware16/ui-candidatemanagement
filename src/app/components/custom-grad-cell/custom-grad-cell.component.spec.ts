import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomGradCellComponent } from './custom-grad-cell.component';
import { AppModule } from 'src/app/app.module';

describe('CustomGradCellComponent', () => {
  let component: CustomGradCellComponent;
  let fixture: ComponentFixture<CustomGradCellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [ CustomGradCellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomGradCellComponent);
    component = fixture.componentInstance;
    //fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
