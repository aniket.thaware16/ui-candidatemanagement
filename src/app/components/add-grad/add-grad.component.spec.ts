import {
  async,
  ComponentFixture,
  TestBed,
  fakeAsync,
} from '@angular/core/testing';

import { AddGradComponent } from './add-grad.component';
import { AppModule } from 'src/app/app.module';
import { MakeHttpService } from 'src/app/service/make-http.service';
import { of } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';

describe('AddGradComponent', () => {
  let component: AddGradComponent;
  let fixture: ComponentFixture<AddGradComponent>;
  let service: MakeHttpService;
  let dialog: MatDialog;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [AddGradComponent],
    }).compileComponents();
    service = TestBed.inject(MakeHttpService);
    dialog = TestBed.inject(MatDialog);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddGradComponent);
    component = fixture.componentInstance;
    //fixture.detectChanges();
  });

  it('should create', () => {
    spyOn(component, 'getAllSkills').and.returnValue();
    component.ngOnInit();
    expect(component).toBeTruthy();
  });

  it('should add grad', () => {
    const dummyGrad = {
      id: 1,
      firstName: 'f4Name',
      middleName: 'm4Name',
      lastName: 'l4Name',
      gender: 'Female',
      email: 'grad4@gmail.com',
      contactNo: '7748965230',
      instituteName: 'College of Engineering,Pune',
      degreeName: 'M.Tech',
      branchName: 'Information Technology',
      joiningDate: '2020-06-02',
      joiningLocation: 'Banglore',
      feedback: '',
      createdOn: new Date('2020-06-11'),
      state: '',
      city: '',
      street: '',
      zipCode: '',
      houseNo: '',
      createdByUser: 'test@accoliteindia.com',
      parentId: 4,
      skills: [
        {
          id: 1,
          skillName: 'Angular',
          createdOn: new Date(),
          createdByUser: 'test@gmail.com',
        },
      ],
    };
    spyOn(component.gradForm, 'value').and.returnValue(dummyGrad);
    spyOn(localStorage, 'getItem').and.returnValue('test@accoliteindia.com');
    spyOn(component.gradForm.get('skills'), 'value').and.returnValue('');
    spyOn(service, 'makePostCall').and.returnValue(of(dummyGrad));
    component.addGrad();
    expect(component.gradAddedMessage).toBe('');
    dialog.closeAll();
  });

  it('should update grad', () => {
    const dummyGrad = {
      id: 1,
      firstName: 'f4Name',
      middleName: 'm4Name',
      lastName: 'l4Name',
      gender: 'Female',
      email: 'grad4@gmail.com',
      contactNo: '7748965230',
      instituteName: 'College of Engineering,Pune',
      degreeName: 'M.Tech',
      branchName: 'Information Technology',
      joiningDate: '2020-06-02',
      joiningLocation: 'Banglore',
      feedback: '',
      createdOn: new Date('2020-06-11'),
      state: '',
      city: '',
      street: '',
      zipCode: '',
      houseNo: '',
      createdByUser: 'test@accoliteindia.com',
      parentId: 4,
      skills: [
        {
          id: 1,
          skillName: 'Angular',
          createdOn: new Date(),
          createdByUser: 'test@gmail.com',
        },
      ],
    };
    component.data = {
      grad: dummyGrad,
    };
    spyOn(component.gradForm, 'value').and.returnValue(dummyGrad);
    spyOn(localStorage, 'getItem').and.returnValue('test@accoliteindia.com');
    spyOn(service, 'makePutCall').and.returnValue(of(dummyGrad));
    spyOn(service, 'makeGetCall').and.returnValue(of(dummyGrad));
    component.updateGrad();
    expect(service.makePutCall).toHaveBeenCalled();
    dialog.closeAll();
  });

  it('should update grad', () => {
    const dummyGrad = {
      id: 1,
      firstName: 'f4Name',
      middleName: 'm4Name',
      lastName: 'l4Name',
      gender: 'Female',
      email: 'grad4@gmail.com',
      contactNo: '7748965230',
      instituteName: 'College of Engineering,Pune',
      degreeName: 'M.Tech',
      branchName: 'Information Technology',
      joiningDate: '2020-06-02',
      joiningLocation: 'Banglore',
      feedback: '',
      createdOn: new Date('2020-06-11'),
      state: '',
      city: '',
      street: '',
      zipCode: '',
      houseNo: '',
      createdByUser: 'test@accoliteindia.com',
      parentId: 4,
      skills: [
        {
          id: 1,
          skillName: 'Angular',
          createdOn: new Date(),
          createdByUser: 'test@gmail.com',
        },
      ],
    };
    component.data = {
      grad: dummyGrad,
      isEdit: true,
    };
    const dummySkills = [
      {
        id: 1,
        skillName: 'Angular',
        createdOn: new Date(),
        createdByUser: 'test@gmail.com',
      },
      {
        id: 2,
        skillName: 'Html',
        createdOn: new Date(),
        createdByUser: 'test@gmail.com',
      },
    ];
    spyOn(service, 'makeGetCall').and.returnValue(of(dummySkills));
    component.getAllSkills();
    expect(component.skills).toBe(dummySkills);
    dialog.closeAll();
  });
});
