import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Skill } from '../../models/skill.model';
import { MakeHttpService } from '../../service/make-http.service';
import { Grad } from '../../models/grad.model';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-add-grad',
  templateUrl: './add-grad.component.html',
  styleUrls: ['./add-grad.component.css']
})
export class AddGradComponent implements OnInit{
  skills: Array<Skill>;
  gradAddedMessage = "";
  allBranch = ["Computer Engineering","Information Technology","Electronics and Telecommunication","Mechanical Engineering","Electronics","Other"];
  allInstitutes = ["Vishwakarma Institute of Technology,Pune","VJTI,Mumbai","College of Engineering,Pune","PICT,Pune","Other"];
  allDegree = ["B.Tech","M.Tech","BSC","MSC","BCA","MCA","Other"];
  allLocation = ["Mumbai","Banglore","Delhi","Chennai","Hyderabad","Other"];
  gradForm = new FormGroup({
    id: new FormControl(''),
    firstName: new FormControl('',[Validators.required,Validators.maxLength(50)]),
    middleName: new FormControl('',[Validators.required,Validators.maxLength(50)]),
    lastName: new FormControl('',Validators.compose([Validators.required,Validators.maxLength(50)])),
    contactNo: new FormControl('',[Validators.required,Validators.min(1000000000), Validators.max(9999999999999)]),
    email: new FormControl('',[Validators.required,Validators.maxLength(255),Validators.email]),
    gender: new FormControl('',[Validators.required]),
    state: new FormControl(''),
    city: new FormControl(''),
    houseNo: new FormControl(''),
    street: new FormControl(''),
    zipCode: new FormControl(''),
    instituteName: new FormControl('',[Validators.required]),
    branchName: new FormControl('',[Validators.required]),
    degreeName: new FormControl('',[Validators.required]),
    skills: new FormControl(''),
    joiningDate: new FormControl(''),
    joiningLocation: new FormControl(''),
    feedback: new FormControl('')
  })
  constructor(private apiCall:MakeHttpService,public dialogRef: MatDialogRef<AddGradComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,private dialog:MatDialog) { }
  ngOnInit(): void {
    this.getAllSkills();
  }
   
  addGrad(){
    let grad: Grad;
    grad = this.gradForm.value;
    grad.createdByUser = localStorage.getItem('email');
    if(this.gradForm.get('skills').value==""){
        grad.skills = Array<Skill>();
    }
    let progressDialog = this.dialog.open(ProgressDialogComponent,{
      width:"400px",
      disableClose: true ,
      data: "Saving Grad"
    })
    this.apiCall.makePostCall(environment.gradUrl,grad).subscribe(result=>{
      progressDialog.close();
      if(result.id != 0){
        this.dialog.closeAll();
      }else{
        this.gradAddedMessage = "Grad '"+ grad.email +"' already added";
      }
    })
  }

  updateGrad(){
    let grad: Grad;
    grad = this.gradForm.value;
    grad.parentId = this.data.grad.parentId;
    grad.createdByUser = localStorage.getItem('email');
    grad.email = this.data.grad.email;
    let updateUrl = environment.gradUrl+"/"+this.data.grad.id;
    let progressDialog = this.dialog.open(ProgressDialogComponent,{
      width:"400px",
      disableClose: true ,
      data: "Updating Grad"
    })
    this.apiCall.makePutCall(updateUrl,grad).subscribe(updateResult=>{
      progressDialog.close();
      this.dialog.closeAll();
    })
  }

  getAllSkills(){
      this.apiCall.makeGetCall(environment.skillUrl).subscribe(allSkills => {
        this.skills = allSkills;
        if(this.data.isEdit){
          this.populateGradForm();
        }
      })
  }

  populateGradForm(){
    this.gradForm.get('firstName').setValue(this.data.grad.firstName);
    this.gradForm.get('middleName').setValue(this.data.grad.middleName)
    this.gradForm.get('lastName').setValue(this.data.grad.lastName)
    this.gradForm.get('contactNo').setValue(this.data.grad.contactNo)
    this.gradForm.get('email').setValue(this.data.grad.email)
    this.gradForm.get('gender').setValue(this.data.grad.gender)
    this.gradForm.get('state').setValue(this.data.grad.state)
    this.gradForm.get('city').setValue(this.data.grad.city)
    this.gradForm.get('houseNo').setValue(this.data.grad.houseNo)
    this.gradForm.get('street').setValue(this.data.grad.street)
    this.gradForm.get('zipCode').setValue(this.data.grad.zipCode)
    this.gradForm.get('instituteName').setValue(this.data.grad.instituteName)
    this.gradForm.get('branchName').setValue(this.data.grad.branchName)
    this.gradForm.get('degreeName').setValue(this.data.grad.degreeName)
    this.gradForm.get('joiningDate').setValue(this.data.grad.joiningDate)
    this.gradForm.get('joiningLocation').setValue(this.data.grad.joiningLocation)
    this.gradForm.get('feedback').setValue(this.data.grad.feedback)
    this.gradForm.get('id').setValue(this.data.grad.id)
    let tempSkill= [];
    for (let skill of this.skills){
      for(let gradSkill of this.data.grad.skills){
        if(skill.id == gradSkill.id){
          tempSkill.push(skill);
        }
      }
    }
    this.gradForm.get('skills').setValue(tempSkill)
    this.gradForm.get('email').disable();
  }

}
