import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomHistoryCellComponent } from './custom-history-cell.component';

describe('CustomHistoryCellComponent', () => {
  let component: CustomHistoryCellComponent;
  let fixture: ComponentFixture<CustomHistoryCellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomHistoryCellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomHistoryCellComponent);
    component = fixture.componentInstance;
   // fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
