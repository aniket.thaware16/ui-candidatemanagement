import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-custom-history-cell',
  templateUrl: './custom-history-cell.component.html',
  styleUrls: ['./custom-history-cell.component.css']
})
export class CustomHistoryCellComponent implements OnInit,ICellRendererAngularComp {
  public params: any;
  constructor() { }
  refresh(params: any): boolean {
    this.params = params;
   return true;
  }
  agInit(params: import("ag-grid-community").ICellRendererParams): void {
    this.params = params;
  }
  

  ngOnInit(): void {
  }

  showDetails(){
    this.params.context.componentParent.showDetailsOfGrad(this.params.value);
  }

}
