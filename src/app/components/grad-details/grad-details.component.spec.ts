import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GradDetailsComponent } from './grad-details.component';
import { AppModule } from 'src/app/app.module';
import { MatTableDataSource } from '@angular/material/table';

describe('GradDetailsComponent', () => {
  let component: GradDetailsComponent;
  let fixture: ComponentFixture<GradDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [ GradDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GradDetailsComponent);
    component = fixture.componentInstance;
  //  fixture.detectChanges();
  });

  it('should create', () => {
    const dummyGrad = {
      id: 1,
      firstName: 'f4Name',
      middleName: 'm4Name',
      lastName: 'l4Name',
      gender: 'Female',
      email: 'grad4@gmail.com',
      contactNo: '7748965230',
      instituteName: 'College of Engineering,Pune',
      degreeName: 'M.Tech',
      branchName: 'Information Technology',
      joiningDate: '2020-06-02',
      joiningLocation: 'Banglore',
      feedback: '',
      createdOn: new  Date ("2020-06-11"),
      state: '',
      city: '',
      street: '',
      zipCode: '',
      houseNo: '',
      createdByUser: 'test@accoliteindia.com',
      parentId: 4,
      skills: [{id:1,
        skillName:"Angular",
        createdOn:new Date(),
    createdByUser:"test@gmail.com"
        }]
    };
    component.data = dummyGrad;
    let dummySkill = ['Angular'];
   let dummyData =  [
      {attribute: "Grad Persional Details",value:""},
      {attribute: "First Name",value:'f4Name'},
      {attribute: "Middle Name",value:'m4Name'},
      {attribute: "Last Name",value:'l4Name'},
      {attribute: "Gender",value:'Female'},
      {attribute: "Email",value:'grad4@gmail.com'},
      {attribute: "Contact Number",value:'7748965230'},
      {attribute: "Academics Details",value:""},
      {attribute: "Institute Name",value:'College of Engineering,Pune'},
      {attribute: "Degree Name",value:'M.Tech'},
      {attribute: "Branch Name",value:'Information Technology'},
      {attribute: "Skills",value:dummySkill.toString()},
      {attribute: "Joining Details",value:""},
      {attribute: "Joining Date",value:'2020-06-02'},
      {attribute: "Joining Location",value:'Banglore'},
      {attribute: "Feedback",value:''},
      {attribute: "Address Details",value:""},
      {attribute: "State",value:''},
      {attribute: "City",value:''},
      {attribute: "Street",value:''},
      {attribute: "Zip Code",value:''},
      {attribute: "House Number",value:''}
    ]
    let dummyDataSource = new MatTableDataSource(dummyData);
    component.ngOnInit();
    expect(component.dataSource.data).toEqual(dummyDataSource.data);
    expect(component).toBeTruthy();
  });
});
