import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Grad } from '../../models/grad.model';

@Component({
  selector: 'app-grad-details',
  templateUrl: './grad-details.component.html',
  styleUrls: ['./grad-details.component.css']
})
export class GradDetailsComponent implements OnInit {
 dataSource: MatTableDataSource<any>;
  constructor(
    public dialogRef: MatDialogRef<GradDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Grad) {}

    displayedColumns: string[] = ['attribute', 'value'];

  
  ngOnInit(): void {
    let skill = []
    for(let tempSkill of this.data.skills){
      skill.push(tempSkill.skillName)
    }
    let  gradDetails = [
      {attribute: "Grad Persional Details",value:""},
      {attribute: "First Name",value:this.data.firstName},
      {attribute: "Middle Name",value:this.data.middleName},
      {attribute: "Last Name",value:this.data.lastName},
      {attribute: "Gender",value:this.data.gender},
      {attribute: "Email",value:this.data.email},
      {attribute: "Contact Number",value:this.data.contactNo},
      {attribute: "Academics Details",value:""},
      {attribute: "Institute Name",value:this.data.instituteName},
      {attribute: "Degree Name",value:this.data.degreeName},
      {attribute: "Branch Name",value:this.data.branchName},
      {attribute: "Skills",value:skill.toString()},
      {attribute: "Joining Details",value:""},
      {attribute: "Joining Date",value:this.data.joiningDate},
      {attribute: "Joining Location",value:this.data.joiningLocation},
      {attribute: "Feedback",value:this.data.feedback},
      {attribute: "Address Details",value:""},
      {attribute: "State",value:this.data.state},
      {attribute: "City",value:this.data.city},
      {attribute: "Street",value:this.data.street},
      {attribute: "Zip Code",value:this.data.zipCode},
      {attribute: "House Number",value:this.data.houseNo}
    ]
    
    this.dataSource = new MatTableDataSource(gradDetails);
  }

}
