import { TestBed, getTestBed } from '@angular/core/testing';

import { AuthGuard } from './auth.guard';
import { AuthService } from 'angularx-social-login';
import { AppModule } from '../app.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from '@angular/router';

describe('AuthGuard', () => {
  let injector: TestBed;
  let authService: AuthService;
  let guard: AuthGuard;
  let routerMock = {navigate: jasmine.createSpy('navigate')}
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthGuard, { provide: Router, useValue: routerMock }],
      imports: [AppModule,HttpClientTestingModule]
    });
   
  injector = getTestBed();
  authService = injector.get(AuthService);
  guard = injector.get(AuthGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
  it('should redirect an unauthenticated user to the login route', () => {
    spyOn(localStorage, 'getItem').and.returnValue("");
    expect(guard.canActivate()).toEqual(false);
    expect(routerMock.navigate).toHaveBeenCalledWith([""]);
  });
 
  it('should allow the authenticated user to access app', () => {
    spyOn(localStorage, 'getItem').and.returnValue("12345");
    expect(guard.canActivate()).toEqual(true);
  });
});
