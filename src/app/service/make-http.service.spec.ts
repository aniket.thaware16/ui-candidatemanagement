import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule,HttpTestingController } from '@angular/common/http/testing';
import { MakeHttpService } from './make-http.service';
import { from, Observable } from 'rxjs';
import { Grad } from '../models/grad.model';
import { environment } from 'src/environments/environment';

describe('MakeHttpService', () => {
  let service: MakeHttpService;
  let httpMock:HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(MakeHttpService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(()=> {
    httpMock.verify();
  })
  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get grads from the get api', () => {
    const dummyGrads:Grad[] = [
      {
        id: 35,
        firstName: 'f7name',
        middleName: 'm7Name',
        lastName: 'l7Name',
        gender: 'Male',
        email: 'grad7@gmail.com',
        contactNo: '7743863042',
        instituteName: 'VJTI,Mumbai',
        degreeName: 'B.Tech',
        branchName: 'Electronics and Telecommunication',
        joiningDate: '2020-06-03',
        joiningLocation: 'Banglore',
        feedback: '',
        createdOn: new  Date ("2020-06-11"),
        state: '',
        city: '',
        street: '',
        zipCode: '',
        houseNo: '',
        createdByUser: 'test@accoliteindia.com',
        parentId: 35,
        skills: null,
      },
      {
        id: 22,
        firstName: 'f5Name',
        middleName: 'm5Name',
        lastName: 'l5Name',
        gender: 'Male',
        email: 'grad6@gmail.com',
        contactNo: '8877442560',
        instituteName: 'College of Engineering,Pune',
        degreeName: 'B.Tech',
        branchName: 'Electronics and Telecommunication',
        joiningDate: '2020-05-01',
        joiningLocation: 'Hyderabad',
        feedback: '',
        createdOn: new  Date ("2020-06-11"),
        state: '',
        city: '',
        street: '',
        zipCode: '',
        houseNo: '',
        createdByUser: 'test@accoliteindia.com',
        parentId: 22,
        skills: null,
      },
      {
        id: 5,
        firstName: 'f5Name',
        middleName: 'm5Name',
        lastName: 'l5Name',
        gender: 'Male',
        email: 'grad5@gmail.com',
        contactNo: '8899774466',
        instituteName: 'Vishwakarma Institute of Technology,Pune',
        degreeName: 'M.Tech',
        branchName: 'Information Technology',
        joiningDate: '2020-06-11',
        joiningLocation: 'Delhi',
        feedback: '',
        createdOn: new  Date ("2020-06-11"),
        state: '',
        city: '',
        street: '',
        zipCode: '',
        houseNo: '',
        createdByUser: 'test@accoliteindia.com',
        parentId: 5,
        skills: null,
      },
      {
        id: 4,
        firstName: 'f4Name',
        middleName: 'm4Name',
        lastName: 'l4Name',
        gender: 'Female',
        email: 'grad4@gmail.com',
        contactNo: '7748965230',
        instituteName: 'College of Engineering,Pune',
        degreeName: 'M.Tech',
        branchName: 'Information Technology',
        joiningDate: '2020-06-02',
        joiningLocation: 'Banglore',
        feedback: '',
        createdOn: new  Date ("2020-06-11"),
        state: '',
        city: '',
        street: '',
        zipCode: '',
        houseNo: '',
        createdByUser: 'test@accoliteindia.com',
        parentId: 4,
        skills: null,
      }
    ];

    service.makeGetCall(environment.gradUrl).subscribe(grads => {
      expect(grads.length).toBe(4);
      expect(grads).toEqual(dummyGrads);
    })

    const request = httpMock.expectOne(environment.gradUrl);
    expect(request.request.method).toBe('GET');
    request.flush(dummyGrads);
  });

  it('should get gradsfrom the post api', () => {
    const dummyGrad:Grad = {
        id: 35,
        firstName: 'f7name',
        middleName: 'm7Name',
        lastName: 'l7Name',
        gender: 'Male',
        email: 'grad7@gmail.com',
        contactNo: '7743863042',
        instituteName: 'VJTI,Mumbai',
        degreeName: 'B.Tech',
        branchName: 'Electronics and Telecommunication',
        joiningDate: '2020-06-03',
        joiningLocation: 'Banglore',
        feedback: '',
        createdOn: new  Date ("2020-06-11"),
        state: '',
        city: '',
        street: '',
        zipCode: '',
        houseNo: '',
        createdByUser: 'test@accoliteindia.com',
        parentId: 35,
        skills: null,
      };

      service.makePostCall(environment.gradUrl,35).subscribe(grad => {
        expect(grad.id).toBe(35);
        expect(grad).toEqual(dummyGrad);
      })
      const request = httpMock.expectOne(environment.gradUrl);
      expect(request.request.method).toBe('POST');
      request.flush(dummyGrad);
  })

  it('should update grad from the put api', () => {
    const dummyGrad:Grad = {
      id: 35,
      firstName: 'f7name',
      middleName: 'm7Name',
      lastName: 'l7Name',
      gender: 'Male',
      email: 'grad7@gmail.com',
      contactNo: '7743863042',
      instituteName: 'VJTI,Mumbai',
      degreeName: 'B.Tech',
      branchName: 'Electronics and Telecommunication',
      joiningDate: '2020-06-03',
      joiningLocation: 'Banglore',
      feedback: '',
      createdOn: new  Date ("2020-06-11"),
      state: '',
      city: '',
      street: '',
      zipCode: '',
      houseNo: '',
      createdByUser: 'test@accoliteindia.com',
      parentId: 35,
      skills: null,
    };
    service.makePutCall(environment.gradUrl,dummyGrad).subscribe(grad => {
      expect(grad.id).toBe(35);
      expect(grad).toEqual(dummyGrad);
    })

    const request = httpMock.expectOne(environment.gradUrl);
    expect(request.request.method).toBe('PUT');
    request.flush(dummyGrad);

  })

  it('should delete grad from the delete api', () => {
    service.makeDeleteCall(environment.gradUrl).subscribe(gradId => {
      expect(gradId).toBe(1);
    })
 
    const requestDelete = httpMock.expectOne(environment.gradUrl);
    expect(requestDelete.request.method).toBe('DELETE');
    requestDelete.flush(1);
  })
});
