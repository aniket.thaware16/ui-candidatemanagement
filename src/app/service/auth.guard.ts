import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'angularx-social-login';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router,private authService:AuthService){}
  canActivate(): boolean {
    if (!!localStorage.getItem('idToken')) {
      return true;
    } else {
      this.router.navigate([""]);
      return false;
    }
  }
  
}
