import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'token': 'my-auth-token'
  })
};
@Injectable({
  providedIn: 'root'
})
export class MakeHttpService {

  constructor(private http:HttpClient) { }

  makeGetCall(url:string):Observable<any>{
    httpOptions.headers =
    httpOptions.headers.set('token', localStorage.getItem('idToken'));
    return this.http.get(url,httpOptions);
  }

  makePostCall(url:string,data:any):Observable<any>{
    httpOptions.headers =
    httpOptions.headers.set('token', localStorage.getItem('idToken'));
    return this.http.post(url,data,httpOptions);
  }

  makePutCall(url:string,data:any):Observable<any> {
    httpOptions.headers =
    httpOptions.headers.set('token', localStorage.getItem('idToken'));
    return this.http.put(url,data,httpOptions);
  }
  makeDeleteCall(url:string):Observable<any>{
    httpOptions.headers =
    httpOptions.headers.set('token', localStorage.getItem('idToken'));
    return this.http.delete(url,httpOptions)
  }
}
