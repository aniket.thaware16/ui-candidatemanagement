import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent, provideConfig } from './components/login/login.component';
import { SocialLoginModule, AuthServiceConfig } from 'angularx-social-login';
import { CandidateManagementComponent } from './components/candidate-management/candidate-management.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { MainNavComponent } from './components/main-nav/main-nav.component';
import { HttpClientModule } from '@angular/common/http';
import { AddGradComponent } from './components/add-grad/add-grad.component';
import { ReactiveFormsModule } from '@angular/forms';
import { GradDetailsComponent } from './components/grad-details/grad-details.component';
import { TrendsComponent } from './components/trends/trends.component';
import { ProgressDialogComponent } from './components/progress-dialog/progress-dialog.component';
import { GradEditedHistoryComponent } from './components/grad-edited-history/grad-edited-history.component';
import { SkillsComponent } from './components/skills/skills.component';
import { AgGridModule } from 'ag-grid-angular';
import { CustomHistoryCellComponent } from './components/custom-history-cell/custom-history-cell.component';
import { CustomGradCellComponent } from './components/custom-grad-cell/custom-grad-cell.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { APP_BASE_HREF } from '@angular/common';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CandidateManagementComponent,
    MainNavComponent,
    AddGradComponent,
    GradDetailsComponent,
    TrendsComponent,
    ProgressDialogComponent,
    GradEditedHistoryComponent,
    SkillsComponent,
    CustomHistoryCellComponent,
    CustomGradCellComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SocialLoginModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    ReactiveFormsModule,
    AgGridModule.withComponents([])
  ],
  entryComponents: [AddGradComponent,GradDetailsComponent,TrendsComponent,ProgressDialogComponent],
  providers: [{provide: AuthServiceConfig,useFactory: provideConfig},{
    provide: MatDialogRef,
    useValue: {}
  },{ provide: MAT_DIALOG_DATA, useValue: {} },
  {provide: APP_BASE_HREF, useValue : '/' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
