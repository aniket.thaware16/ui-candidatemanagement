import { Skill } from './skill.model';

export interface Grad {
    id: number;
    firstName:string;
    middleName:string;
    lastName:string;
    gender:string;
    email:string;
    contactNo:string;
    instituteName:string;
    degreeName:string;
    branchName:string;
    joiningDate:string;
    joiningLocation:string;
    feedback:string;
    createdOn:Date;
    state:string;
    city:string;
    street:string;
    zipCode:string;
    houseNo:string;
    createdByUser:string;
    parentId:number;
    skills:Array<Skill>;
}