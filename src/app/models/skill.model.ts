export interface Skill{
    id:number;
    skillName:string;
    createdOn:Date;
    createdByUser:string;
}